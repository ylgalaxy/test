export default {
	change (value) {
		if (value == 'Blue') {
			Tabs1.setVisibility(true);
			Tabs2.setVisibility(false);
		} else if (value == 'Green') {
			Tabs2.setVisibility(true);
			Tabs1.setVisibility(false);
		}
	},
}